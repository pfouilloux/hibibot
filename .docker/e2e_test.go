package _docker

import (
	"context"
	"flag"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"io"
	"strings"
	"sync"
	"testing"
)

var image = flag.String("image", "pfxdev/hibibot:local", "image to test with")

func TestE2E(t *testing.T) {
	tests := map[string]struct {
		cmd      []string
		expected []string
	}{
		"should have dependencies installed": {
			cmd:      []string{"cargo", "install", "--list"},
			expected: []string{"cargo-edit v0.7.0"},
		},
	}

	for name, test := range tests {
		test := test

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ctx := context.Background()
			cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
			if err != nil {
				t.Fatalf("failed to create docker client")
			}

			resp, err := cli.ContainerCreate(ctx, &container.Config{
				Image: *image,
				Cmd:   test.cmd,
				Tty:   false,
			}, nil, nil, nil, "")
			if err != nil {
				t.Fatalf("failed to create container: %v\nimage: %s cmd: %v", err, *image, test.cmd)
			}

			defer func() {
				_ = cli.ContainerRemove(ctx, resp.ID, types.ContainerRemoveOptions{})
			}()

			if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
				t.Fatalf("failed to start container %s", resp.ID)
			}

			statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)
			select {
			case err := <-errCh:
				if err != nil {
					t.Fatalf("container failed unexpectedly: %v", err)
				}
			case <-statusCh:
			}

			out, err := cli.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{ShowStdout: true, ShowStderr: true})
			if err != nil {
				t.Fatalf("failed to get container logger: %v", err)
			}

			logs, err := io.ReadAll(out)
			if err != nil {
				t.Fatalf("failed to read container logger: %v", err)
			}

			once := sync.Once{}
			var failures strings.Builder
			for _, expect := range test.expected {
				if !strings.Contains(string(logs), expect) {
					once.Do(func() {
						failures.WriteString(fmt.Sprintf(
							"expected logger:\n%sto contain:\n\t- %s\n%s",
							strings.Join([]string{string(logs)}, "\n"),
							strings.Join(test.expected, "\n\t- "),
							"missing:",
						))
					})

					failures.WriteString(fmt.Sprintf("\n\t- %s", expect))
				}
			}

			if failures.Len() > 0 {
				t.Error(failures.String())
			}
		})
	}
}
