IMAGE_NAME=pfxdev/hibibot
IMAGE_VERSION=local
TEST?=$(go list ./...)

.PHONY: check
check: test lint docker-test

.PHONY: test
test: unit-test

.PHONY: lint
lint:
	@go install honnef.co/go/tools/cmd/staticcheck@latest
	@staticcheck -f stylish ./...

.PHONY: unit-test
unit-test:
	go test ./...

.PHONY: docker-test
docker-test: docker-build
	go test ./.docker -image ${IMAGE_NAME}:${IMAGE_VERSION}

.PHONY: docker-build
docker-build:
	docker build -t ${IMAGE_NAME}:${IMAGE_VERSION} --build-arg BUILDKIT_INLINE_CACHE=1 -f ./.docker/Dockerfile .

.PHONY: package
package:
	go build -o dist/

.PHONY: fmt
fmt:
	@echo formatting...
	@gofmt -s -w .