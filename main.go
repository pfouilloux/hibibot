package main

import (
	"hibibot/internal/cli"
	"hibibot/internal/logger"
	"hibibot/internal/subcmds"
	"os"
)

func main() {
	log := logger.New(os.Stdout)
	app := cli.New(log,
		subcmds.NewSetup(log, "cargo-edit"),
	)
	app.Run(os.Args[1:])
}
