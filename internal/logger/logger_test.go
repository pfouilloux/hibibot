package logger

import (
	"os/exec"
	"strings"
	"testing"
)

func TestDebug(t *testing.T) {
	tests := map[string]struct {
		verbose bool
		input   string
		expect  string
	}{
		"should not print when verbose is disabled": {
			verbose: false,
			input:   "I shouldn't be here...",
		},
		"should print when verbose is enabled": {
			verbose: true,
			input:   "Hello world!",
			expect:  "logger_test.go:34 Hello world!\n",
		},
	}

	for name, test := range tests {
		test := test
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			var buf strings.Builder
			logger := New(&buf)
			logger.SetVerbose(test.verbose)
			logger.Debugf("%s", test.input)

			if test.expect != buf.String() {
				t.Fatalf("output mismatch\nexpected: %s\n but got: %s", test.expect, buf.String())
			}
		})
	}
}

func TestShouldPipeCommandOutput(t *testing.T) {
	var oBuf strings.Builder

	cmd := exec.Command("test")

	output := New(&oBuf)

	output.PipeCmdOutput(cmd)

	if cmd.Stdout != &oBuf {
		t.Error("expected the commands stdout to point to the out buffer")
	}
	if cmd.Stderr != &oBuf {
		t.Error("expected the commands stderr to point to the err buffer")
	}
}
