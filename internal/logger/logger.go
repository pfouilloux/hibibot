package logger

import (
	"fmt"
	"io"
	"log"
	"os/exec"
	"runtime"
)

type Logger struct {
	*log.Logger
	verbose bool
}

func New(writer io.Writer) *Logger {
	return &Logger{
		Logger: log.New(writer, "", 0),
	}
}

func (l *Logger) Debugf(format string, args ...interface{}) {
	if l.verbose {
		file, line := caller()
		l.Printf("%s:%d %s", file, line, fmt.Sprintf(format, args...))
	}
}

func (l *Logger) PipeCmdOutput(cmd *exec.Cmd) {
	cmd.Stderr = l.Writer()
	cmd.Stdout = l.Writer()
}

func (l *Logger) SetVerbose(verbose bool) {
	l.verbose = verbose
}

func caller() (file string, line int) {
	_, file, line, _ = runtime.Caller(2)

	short := file
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			short = file[i+1:]
			break
		}
	}
	file = short

	return file, line
}
