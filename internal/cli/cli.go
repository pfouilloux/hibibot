package cli

import (
	"fmt"
	"hibibot/internal/logger"
	"hibibot/internal/subcmds"
)

type cli struct {
	log           *logger.Logger
	subCmdsByName map[string]subcmds.SubCmd
}

func New(log *logger.Logger, subCmds ...subcmds.SubCmd) *cli {
	subCmdsByName := map[string]subcmds.SubCmd{}
	for _, cmd := range subCmds {
		subCmdsByName[cmd.Name()] = cmd
	}

	return &cli{
		log,
		subCmdsByName,
	}
}

func (c *cli) Run(args []string) {
	if len(args) < 1 {
		c.printUsage("please supply a command")
		return
	}

	cmd, ok := c.subCmdsByName[args[0]]
	if !ok {
		c.printUsage(fmt.Sprintf("unknown command '%s'", args[0]))
		return
	}

	if err := cmd.Execute(args[1:]...); err != nil {
		c.log.Printf("failed to execute '%s': %v", cmd.Name(), err)
	}
}

func (c *cli) printUsage(msg string) {
	c.log.Printf("%s\nusage hibibot [command] [options]\nvalid commands:", msg)

	for _, cmd := range c.subCmdsByName {
		c.log.Printf("\t%s - %s\n", cmd.Name(), cmd.Describe())
	}
}
