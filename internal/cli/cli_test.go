package cli

import (
	"errors"
	"fmt"
	"hibibot/internal/logger"
	"io"
	"regexp"
	"strings"
	"testing"
)

func TestAcceptsCommands(t *testing.T) {
	tests := map[string]struct {
		command *stubCommand
		input   []string
		expect  string
	}{
		"Should accept valid command": {
			command: &stubCommand{
				name:        "greet",
				description: "greets",
				exec: func(w io.Writer) error {
					_, err := fmt.Fprint(w, "hello world!")
					return err
				}},
			input:  []string{"greet"},
			expect: "hello world!",
		},
		"Should refuse empty command and print help": {
			command: &stubCommand{name: "greet", description: "greets the user"},
			expect:  "please supply a command\nusage hibibot [command] [options]\nvalid commands:\n\tgreet - greets the user\n",
		},
		"Should refuse invalid command and print help": {
			command: &stubCommand{name: "greet", description: "greets the user"},
			input:   []string{"fail"},
			expect:  "unknown command 'fail'\nusage hibibot [command] [options]\nvalid commands:\n\tgreet - greets the user\n",
		},
		"Should print out error": {
			command: &stubCommand{name: "error", exec: func(_ io.Writer) error { return errors.New("flop") }},
			input:   []string{"error"},
			expect:  "failed to execute 'error': flop\n",
		},
	}

	for name, test := range tests {
		test := test
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			buf := strings.Builder{}
			test.command.out = &buf

			cli := New(logger.New(&buf), test.command)
			cli.Run(test.input)

			if test.expect != buf.String() {
				re := regexp.MustCompile(`\r?\n`)
				expect := re.ReplaceAllString(test.expect, "\\n")
				actual := re.ReplaceAllString(buf.String(), "\\n")
				t.Errorf("output mismatch\nexpected: %s\n but got: %s", expect, actual)
			}
		})
	}
}

type stubCommand struct {
	out         io.Writer
	name        string
	description string
	exec        func(w io.Writer) error
}

func (s *stubCommand) Name() string {
	return s.name
}

func (s *stubCommand) Describe() string {
	return s.description
}

func (s *stubCommand) Execute(args ...string) error {
	return s.exec(s.out)
}
