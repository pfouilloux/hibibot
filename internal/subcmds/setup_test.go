package subcmds

import (
	"fmt"
	"hibibot/internal/logger"
	"os"
	"os/exec"
	"strings"
	"testing"
)

func TestSetup(t *testing.T) {
	tests := map[string]struct {
		cargoPackages      []string
		expectedPackageIds []string
		expectedErr        string
	}{
		"Should install package": {
			cargoPackages:      []string{"hello"},
			expectedPackageIds: []string{"hello v0.1.0"},
		},
		"Should fail to install nonexistent package": {
			cargoPackages: []string{"nope"},
			expectedErr:   "'cargo install --path testdata/nope' execution failed: exit status 101",
		},
	}

	for name, test := range tests {
		test := test
		cleanup(test.cargoPackages)
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			defer cleanup(test.cargoPackages)
			testDataPkgs := make([]string, len(test.cargoPackages))
			for i, pkg := range test.cargoPackages {
				testDataPkgs[i] = fmt.Sprintf("--path testdata/%s", pkg)
			}

			setup := NewSetup(logger.New(os.Stdout), testDataPkgs...)

			if err := setup.Execute(); err != nil && err.Error() != test.expectedErr {
				t.Fatalf("unexpected error running setup\nexpected: %v\n but got: %v", test.expectedErr, err)
			}

			out, err := exec.Command("cargo", "install", "--list").Output()
			if err != nil {
				t.Fatalf("failed to get list of installed packages")
			}

			installed := string(out)
			for _, pkg := range test.expectedPackageIds {
				if !strings.Contains(installed, pkg) {
					t.Fatalf("did not find 'testdata/%s' in installed packages", pkg)
				}
			}
		})
	}
}

func cleanup(cargoPackages []string) {
	for _, pkg := range cargoPackages {
		_ = exec.Command("cargo", "uninstall", pkg).Run()
	}
}
