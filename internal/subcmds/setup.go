package subcmds

import (
	"fmt"
	"hibibot/internal/logger"
	"os/exec"
	"strings"
)

type setupCmd struct {
	log     *logger.Logger
	plugins []string
}

func NewSetup(log *logger.Logger, plugins ...string) SubCmd {
	return newSubCmd("setup", "installs required dependencies", &setupCmd{log, plugins})
}

func (s *setupCmd) execute() error {
	for _, plugin := range s.plugins {
		args := append([]string{"install"}, strings.Split(plugin, " ")...)

		cmd := exec.Command("cargo", args...)
		s.log.PipeCmdOutput(cmd)

		if err := cmd.Run(); err != nil {
			return fmt.Errorf("'%s' execution failed: %w", strings.Join(cmd.Args, " "), err)
		}
	}

	return nil
}
