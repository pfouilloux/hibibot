package subcmds

import (
	"flag"
	"testing"
)

func TestShouldName(t *testing.T) {
	sub := newSubCmd("test", "", nil)

	expect := "test"
	if expect != sub.Name() {
		t.Fatalf("name did not match\nexpected: %s\n but got:%s", expect, sub.Name())
	}
}

func TestShouldDescribe(t *testing.T) {
	sub := newSubCmd("", "hello world!", nil)

	expect := "hello world!"
	if expect != sub.Describe() {
		t.Fatalf("description did not match\nexpected: %s\n but got:%s", expect, sub.Name())
	}
}

func TestShouldExecute(t *testing.T) {
	tests := map[string]struct {
		flags         func(fs *flag.FlagSet)
		input         []string
		expectedFlags map[string]string
		expectedErr   string
	}{
		"should execute with flags": {
			flags:         func(fs *flag.FlagSet) { fs.String("test", "", "") },
			input:         []string{"-test", "hello"},
			expectedFlags: map[string]string{"test": "hello"},
		},
		"should error on unknown flag": {
			flags:         func(fs *flag.FlagSet) { fs.String("test", "", "") },
			input:         []string{"-scoop"},
			expectedFlags: map[string]string{},
			expectedErr:   "flag provided but not defined: -scoop",
		},
	}

	for name, test := range tests {
		test := test
		t.Run(name, func(t *testing.T) {
			sub := newSubCmd("", "", testExecutor{test.flags})

			err := sub.Execute(test.input...)

			switch {
			case test.expectedErr == "" && err != nil:
				t.Fatalf("unexpected error: %v", err)
			case test.expectedErr != "" && (err == nil || err.Error() != test.expectedErr):
				t.Fatalf("error mismatch\nexpected: %v\n but got: %v", test.expectedErr, err)
			}

			if test.expectedFlags == nil || sub.flags.NFlag() > len(test.expectedFlags) {
				t.Fatalf("%d more flags than expected\nexpected: %v\n but got: %v",
					sub.flags.NFlag()-len(test.expectedFlags),
					test.expectedFlags,
					test.input)
			}
			for name, expect := range test.expectedFlags {
				f := sub.flags.Lookup(name)
				switch {
				case f == nil:
					t.Fatalf("flag '%s' not found", name)
				case expect != f.Value.String():
					t.Fatalf("'%s' flag value mismatch\nexpected: %s\n but got:%s", name, expect, f.Value.String())
				}
			}

			if sub.flags.NArg() != 0 {
				t.Fatalf("some flags were not parsed: %v", sub.flags.Args())
			}
		})
	}
}

type testExecutor struct {
	flags func(fs *flag.FlagSet)
}

func (t testExecutor) setFlags(fs *flag.FlagSet) {
	if t.flags != nil {
		t.flags(fs)
	}
}

func (t testExecutor) execute() error {
	return nil
}
