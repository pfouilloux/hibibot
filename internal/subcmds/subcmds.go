package subcmds

import (
	"flag"
)

type SubCmd interface {
	Name() string
	Describe() string
	Execute(args ...string) error
}

type executor interface {
	execute() error
}

type withFlags interface {
	executor
	setFlags(fs *flag.FlagSet)
}

type subCmd struct {
	exec        executor
	flags       *flag.FlagSet
	description string
}

func newSubCmd(name, description string, exec executor) *subCmd {
	return &subCmd{exec, flag.NewFlagSet(name, flag.ContinueOnError), description}
}

func (s *subCmd) Name() string {
	return s.flags.Name()
}

func (s *subCmd) Describe() string {
	return s.description
}

func (s *subCmd) Execute(args ...string) error {
	if flagExec, ok := interface{}(s.exec).(withFlags); ok {
		flagExec.setFlags(s.flags)
	}

	if err := s.flags.Parse(args); err != nil {
		return err
	}

	return s.exec.execute()
}
